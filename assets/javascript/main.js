const DRUMS = {
    keys: document.querySelectorAll('.key'),
    removeTransition: function(event) {
      if(event.propertyName != 'transform') return;
      this.classList.remove('playing');
    }
};

(function() {
    window.addEventListener('keydown', function(event) {
        const key = document.querySelector(`div[data-key="${event.keyCode}"]`);
        const audio = document.querySelector(`audio[data-key="${event.keyCode}"]`);

        if(audio == null) return;
        
        // set audio to play from start
        audio.currentTime = 0;
        audio.play();

        key.classList.add('playing');
    });

    DRUMS.keys.forEach(function(key) {
    key.addEventListener('transitionend', DRUMS.removeTransition);
    });
})();